﻿using System;

namespace JP.Infrastructure
{
    /// <summary>
    /// Интерфейс для работы с различными Repository под одним Entity контекстом
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Возвращает Repository по заданному Entity контексту 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        /// <summary>
        /// Сохраняет контекст в БД
        /// </summary>
        /// <returns></returns>
        int Complete();
    }
}