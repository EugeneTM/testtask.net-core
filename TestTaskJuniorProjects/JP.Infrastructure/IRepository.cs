﻿using System;
using System.Linq;

namespace JP.Infrastructure
{
    /// <summary>
    /// Repository по работе с сущностями базы данных
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Добавляет новую запись
        /// </summary>
        /// <param name="obj"></param>
        void Add(TEntity obj);

        /// <summary>
        /// Возвращает запись по заданному идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns></returns>
        TEntity GetById(int id);

        /// <summary>
        /// Возвращает все записи
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Обновляет запись в БД
        /// </summary>
        /// <param name="obj"></param>
        void Update(TEntity obj);

        /// <summary>
        /// Удаляет запись по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        void Remove(int id);

        /// <summary>
        /// Удаляет запись в БД
        /// </summary>
        void Remove(TEntity obj);

        /// <summary>
        /// Сохраняет измененный контекст в базу данных
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}