﻿using Microsoft.AspNetCore.Identity;
using TestTaskJuniorProjects.Models;

namespace TestTaskJuniorProjects.Seed
{
    /// <summary>
    /// Класс для создания пользователя
    /// </summary>
    public class InitUser
    {
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="userManager">Предоставляет API для управления пользователем в постоянном хранилище.</param>
        /// <param name="roleManager">Предоставляет API-интерфейсы для управления ролями в постоянном хранилище.</param>
        public static void Init(
            UserManager<AppUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            CreateRole(roleManager);
            CreateUser(userManager);
        }

        /// <summary>
        /// Создание пользователя
        /// </summary>
        /// <param name="userManager">Предоставляет API для управления пользователем в постоянном хранилище</param>
        private static void CreateUser(UserManager<AppUser> userManager)
        {
            if (userManager.FindByNameAsync("admin").Result != null) return;
            var user = new AppUser()
            {
                Login = "admin",
                Email = "testAdmin@test.ru",
                UserName = "admin"
            };

            var result = userManager.CreateAsync(user, "123").Result;
                
            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(user, "admin").Wait();
            }
        }

        /// <summary>
        /// Создание роли
        /// </summary>
        /// <param name="roleManager">Предоставляет API-интерфейсы для управления ролями в постоянном хранилище</param>
        private static void CreateRole
            (RoleManager<IdentityRole> roleManager)
        {
            if (roleManager.RoleExistsAsync("Admin").Result) return;
            var role = new IdentityRole();
            role.Name = "Admin";
            var roleResult = roleManager.CreateAsync(role).Result;
        }
    }
}