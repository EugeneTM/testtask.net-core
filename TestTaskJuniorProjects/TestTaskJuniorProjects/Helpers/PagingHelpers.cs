﻿using System;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestTaskJuniorProjects.Util;
using TagBuilder = System.Web.Mvc.TagBuilder;

namespace TestTaskJuniorProjects.Helpers
{
    public static class PagingHelpers
    {
        public static HtmlString PageLinks(this IHtmlHelper html,
            PageBaseModel pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPage; i++)
            {
                TagBuilder tag = new TagBuilder("a");

                tag.MergeAttribute("href", pageUrl(i));
                tag.SetInnerText(i.ToString());

                if (i == pageInfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }

                tag.AddCssClass("btn btn-dark");
                result.Append(tag.ToString());
            }

            return new HtmlString(result.ToString());
        }
    }
}