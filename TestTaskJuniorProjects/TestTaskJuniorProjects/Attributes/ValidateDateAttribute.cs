﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestTaskJuniorProjects.Attributes
{
    /// <summary>
    /// Атрибут для валидации даты
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter,
        AllowMultiple = false)]
    public class ValidateDateAttribute : ValidationAttribute
    {
        public ValidateDateAttribute(string errorMessage) : base(errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public override bool IsValid(object value)
        {
            DateTime validateDate = value is DateTime ? (DateTime) value : default;

            if (value != null && (validateDate != default))
                return true;

            return false;
        }
    }
}