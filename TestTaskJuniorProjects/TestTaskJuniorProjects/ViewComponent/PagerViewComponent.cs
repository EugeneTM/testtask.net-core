﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTaskJuniorProjects.Util;

namespace TestTaskJuniorProjects.ViewComponent
{
    /// <summary>
    /// Компонент для работы м пагинацией
    /// </summary>
    public class PagerViewComponent : Microsoft.AspNetCore.Mvc.ViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(PagedBase result)
        {
            return Task.FromResult((IViewComponentResult)View("Default", result));
        }
    }
}