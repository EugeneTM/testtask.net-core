﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskJuniorProjects.Models
{
    public class Entity
    {
        /// <summary>
        /// ID 
        /// </summary>
        [Key]
        public int ID { get; set; }
    }
}