﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestTaskJuniorProjects.Models
{
    /// <summary>
    /// Модель Фильма
    /// </summary>
    public class Film : Entity
    {
        /// <summary>
        /// Наименование фильма
        /// </summary>        
        public string Name { get; set; }
        
        /// <summary>
        /// Режесёр 
        /// </summary>        
        public string Director { get; set; }
        
        /// <summary>
        /// Дата выхода
        /// </summary>        
        public DateTime ReleaseDate { get; set; }
        
        /// <summary>
        /// Постер
        /// </summary>
        public string ImgUrl { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// ID Пользователя
        /// </summary>
        public string UserId { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        public AppUser User { get; set; }
       
    }
}