﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace TestTaskJuniorProjects.Models
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class AppUser : IdentityUser
    {
        /// <summary>
        /// Логин
        /// </summary>
        [Display(Name = "Логин")]
        public string Login { get; set; }
        
        public List<Film> Films { get; set; }
    }
}