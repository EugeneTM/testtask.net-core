﻿using System;
using System.Linq;
using TestTaskJuniorProjects.Util;

namespace TestTaskJuniorProjects.Extensions
{
    /// <summary>
    /// Вспомогательный класс для работы с пагинацией
    /// </summary>
    public static class PageExtensions
    {
        /// <summary>
        /// Получение пвгинации
        /// </summary>
        /// <param name="query">Тип расширения</param>
        /// <param name="page">Страница</param>
        /// <param name="pageSize">Количество</param>
        /// <typeparam name="T">Модель</typeparam>
        /// <returns></returns>
        public static PagedResult<T> GetPaged<T>(this IQueryable<T> query,
            int page, int pageSize) where T : class
        {
            var result = new PagedResult<T>();
            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = query.Count();

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }
    }
}