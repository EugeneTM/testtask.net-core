﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace TestTaskJuniorProjects.Extensions
{
    /// <summary>
    /// Вспомогательный каласс для работы с миграциями
    /// </summary>
    public static class DbContextAppBuilderExtensions
    {
        /// <summary>
        /// Выполняет автоматическую миграцию БД при запуске приложения
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="app"></param>
        public static void EnsureMigrationOfContext<T>(this IApplicationBuilder app) where T : DbContext
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()
                .CreateScope();
            var context = serviceScope.ServiceProvider.GetService<T>();
            context.Database.Migrate();
        }
    }
}