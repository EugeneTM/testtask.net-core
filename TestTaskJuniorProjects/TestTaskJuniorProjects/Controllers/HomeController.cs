﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using JP.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TestTaskJuniorProjects.Extensions;
using TestTaskJuniorProjects.Models;
using TestTaskJuniorProjects.ViewModel;

namespace TestTaskJuniorProjects.Controllers
{
    /// <summary>
    /// Контроллер для работы с фильмами
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IManagerMVC _manager;

        public HomeController(UserManager<AppUser> userManager, IManagerMVC manager,
            SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _manager = manager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// Главная страница
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns></returns>
        public IActionResult Index(int page = 1)
        {
            try
            {
                var films = _manager.GetFilmsPagedResult(page, 16);

                if (_signInManager.IsSignedIn(User))
                {
                    var id = this.User.GetUserId();
                    ViewData["UserID"] = id;
                }

                return View( new FilmViewModel()
                {
                    Films = films
                });
            }
            catch (Exception e)
            {
                throw new Exception($"Что то пошло не так (. - [{e.StackTrace}]");
            }
        }

        /// <summary>
        /// Старница Фильма
        /// </summary>
        /// <param name="id">ID записи</param>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var film = await _manager.GetFilmByID(id);
            if (film == null)
            {
                return NotFound();
            }
            
            var model = CreateModel(film);

            return View(model);
        }

        /// <summary>
        /// Редактирование
        /// </summary>
        /// <param name="id">ID записи</param>
        /// <param name="isEdit"></param>
        [Authorize]
        public async Task<IActionResult> AddOrEdit(int? id, bool isEdit)
        {
            ViewData["isEdit"] = isEdit;
            
            if (!isEdit)
                return View(new FilmVm()
                {
                    ReleaseDate = DateTime.Now
                });
            
            var film = await _manager.GetFilmByID(id);
            
            if (id == null || film == null)            
                return NotFound();

            var model = CreateModel(film);
            
            return View(model);
        }

        /// <summary>
        /// Редактирование
        /// </summary>
        /// <param name="id">ID записи</param>
        /// <param name="film">Модель фильма</param>
        /// <param name="img">Модель для загрузки img</param>
        /// <param name="isEdit"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> AddOrEdit(int id, FilmVm film, Img img, bool isEdit)
        {
            var user = await _userManager.GetUserAsync(User);
            
            if (id != film.ID)
                return NotFound();

            if (isEdit && !_manager.FilmExists(film.ID))
                return NotFound();

            if (isEdit && !ValidateEditUser(film.UserId))
                return RedirectToAction(nameof(Index));

            if (ModelState.IsValid)
            {
                if (isEdit)
                    await _manager.EditFilm(film, img, id);
                else
                    await _manager.CreateFilm(film, img, user.Id);
                
                return RedirectToAction(nameof(Index));
            }

            return View(film);
        }

        /// <summary>
        /// Проверка на возможность редактировать
        /// </summary>
        /// <param name="userId">ID Пользльователя</param>
        /// <returns></returns>
        private bool ValidateEditUser(string userId) => userId == _userManager.GetUserAsync(User).Result.Id;
        

        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="id">ID записи</param>
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();
            
            var film = await _manager.GetFilmByID(id);

            if (film == null) 
                return NotFound();
            
            await _manager.RemoveFilm(id);
            
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Создание модели
        /// </summary>
        /// <param name="film"></param>
        /// <returns></returns>
        private FilmVm CreateModel(Film film) => new FilmVm()
        {
            Description = film.Description,
            Name = film.Name,
            Director = film.Director,
            ReleaseDate = film.ReleaseDate,
            User = _manager.GetUser(film.UserId),
            UserId = film.UserId,
            ID = film.ID,
            ImgUrl = film.ImgUrl
        };
        
        /// <summary>
        /// Страница ошибки
        /// </summary>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}