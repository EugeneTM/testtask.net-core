using JP.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using TestTaskJuniorProjects.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TestTaskJuniorProjects.Data.Configuration;
using TestTaskJuniorProjects.Extensions;
using TestTaskJuniorProjects.Manager;
using TestTaskJuniorProjects.Models;
using TestTaskJuniorProjects.Seed;
using TestTaskJuniorProjects.Util;

namespace TestTaskJuniorProjects
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]), 
                ServiceLifetime.Transient, ServiceLifetime.Transient);
           
            services.AddIdentity<AppUser, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 3;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddDefaultUI();

            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddScoped<FileUploadService>();
            
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IManagerMVC, ManagerMVC>();
            services.AddSingleton(sp => new EntityConfigurations());
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.EnsureMigrationOfContext<ApplicationDbContext>();
            
            initDb(app);
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
           
        }

        /// <summary>
        /// Инициализация данных DB
        /// </summary>
        private void initDb(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.GetService<IServiceScopeFactory> ().CreateScope ();
            
            try
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                var uow = new UnitOfWork(context);
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                InitUser.Init(userManager, roleManager);

                InitData.Init(uow);
            }
            catch
            {
                // ignored
            }
        }
        
    }
}