﻿using System.Collections.Generic;

namespace TestTaskJuniorProjects.Util
{
    /// <summary>
    /// Класс для формирования пагинации
    /// </summary>
    /// <typeparam name="T">Тип ограничения</typeparam>
    public class PagedResult<T> : PagedBase where T : class
    {
        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}