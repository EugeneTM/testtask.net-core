﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace TestTaskJuniorProjects.Util
{
    /// <summary>
    /// Класс для загрузки img
    /// </summary>
    public class FileUploadService
    {
        public FileUploadService()
        {
            
        }
        
        /// <summary>
        /// Загрузка img
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="fileName">Имя файла</param>
        /// <param name="file">Файл</param>
        public async void Upload(string path, string fileName, IFormFile file)
        {
            Directory.CreateDirectory(path);
            await using var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create);
            await file.CopyToAsync(stream);
        }
        
        /// <summary>
        /// Удаляем img
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        public void Delete(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
    }
}