﻿using System;

namespace TestTaskJuniorProjects.Util
{
    public class PageBaseModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }

        public int TotalPage
        {
            get { return (int) Math.Ceiling((decimal) TotalItems / PageSize); }
        }
    }
}