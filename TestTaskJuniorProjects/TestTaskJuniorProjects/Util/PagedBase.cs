﻿using System;

namespace TestTaskJuniorProjects.Util
{
    /// <summary>
    /// Базовый класс для пагинации
    /// </summary>
    public class PagedBase
    {
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int CurrentPage { get; set; }
        
        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageCount { get; set; }
        
        /// <summary>
        /// Размер страницы
        /// </summary>
        public int PageSize { get; set; }
        
        /// <summary>
        /// Количество строк
        /// </summary>
        public int RowCount { get; set; }

        /// <summary>
        /// Первая строка на странице
        /// </summary>
        public int FirstRowOnPage
        {
            get { return (CurrentPage - 1) * PageSize + 1; }
        }

        /// <summary>
        /// Последняя строка на странице
        /// </summary>
        public int LastRowOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, RowCount); }
        }
    }
}