﻿using System;
using System.Collections.Concurrent;
using JP.Infrastructure;

namespace TestTaskJuniorProjects.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Контекст БД
        /// </summary>
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Кэш Repository
        /// </summary>
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return _repositories.GetOrAdd(typeof(TEntity), x => new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns></returns>
        public int Complete()
        {
            return _context.SaveChanges();
        }
    }
}