﻿using System;
using System.Linq;
using JP.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace TestTaskJuniorProjects.Data
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected ApplicationDbContext Db;
        protected DbSet<TEntity> DbSet;

        public Repository(ApplicationDbContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Add(TEntity obj)
        {
            DbSet.Add(obj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Update(TEntity obj)
        {
            DbSet.Update(obj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Remove(TEntity obj)
        {
            DbSet.Remove(obj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}