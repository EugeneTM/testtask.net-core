﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TestTaskJuniorProjects.Data.Configuration;
using TestTaskJuniorProjects.Models;

namespace TestTaskJuniorProjects.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        private readonly EntityConfigurations _entityConfigurations;

        public DbSet<Film> Films { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, EntityConfigurations entityConfigurations)
            : base(options)
        {
            _entityConfigurations = entityConfigurations;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity(_entityConfigurations.ProvidersFilmConfiguration.ProvideApplyAction());
        }
    }
}