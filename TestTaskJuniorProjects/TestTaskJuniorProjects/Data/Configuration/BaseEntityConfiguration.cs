﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTaskJuniorProjects.Data.Configuration
{
    /// <summary>
    /// Базовый класс для настроек конфигурации сущностей БД
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseEntityConfiguration<T> : IEntityConfiguration<T> where T : class
    {
        public Action<EntityTypeBuilder<T>> ProvideApplyAction()
        {
            return builder =>
            {
                ConfigureProperties(builder);
                ConfigurePrimaryKeys(builder);
                ConfigureForeignKeys(builder);
            };
        }

        protected abstract void ConfigureProperties(EntityTypeBuilder<T> builder);
        protected abstract void ConfigurePrimaryKeys(EntityTypeBuilder<T> builder);
        protected virtual void ConfigureForeignKeys(EntityTypeBuilder<T> builder) { }
    }
}