﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTaskJuniorProjects.Data.Configuration
{
    /// <summary>
    /// Конфигурация для entity сущностей
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntityConfiguration<T> where T : class
    {
        Action<EntityTypeBuilder<T>> ProvideApplyAction();
    }
}