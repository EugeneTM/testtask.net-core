﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskJuniorProjects.Models;

namespace TestTaskJuniorProjects.Data.Configuration.DefaultFluentApi
{
    public class ProvidersFilmConfiguration : BaseEntityConfiguration<Film>
    {
        /// <summary>
        /// Настройка entity для Заявок по быстрым переводам
        /// </summary>
        /// <param name="builder"></param>
        protected override void ConfigureProperties(EntityTypeBuilder<Film> builder)
        {
            builder.HasOne<AppUser>()
                .WithMany(x => x.Films)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.ToTable("Films");
            
            builder.Property(c => c.ID)
                .ValueGeneratedOnAdd();
            
            builder.Property(c => c.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(c => c.Director)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(c => c.ReleaseDate)
                .HasColumnType("DATE");

            builder.Property(c => c.ImgUrl)
                .HasMaxLength(500)
                .IsRequired();
            
            builder.Property(c => c.Description)
                .HasMaxLength(500)
                .IsRequired(false);
        }

        protected override void ConfigurePrimaryKeys(EntityTypeBuilder<Film> builder)
        {
            builder.HasKey(x => new {x.ID});
        }
    }
}