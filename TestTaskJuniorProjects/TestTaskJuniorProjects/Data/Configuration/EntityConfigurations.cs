﻿using TestTaskJuniorProjects.Data.Configuration.DefaultFluentApi;
using TestTaskJuniorProjects.Models;

namespace TestTaskJuniorProjects.Data.Configuration
{
    public class EntityConfigurations
    {
        public IEntityConfiguration<Film> ProvidersFilmConfiguration { get; set; }

        public EntityConfigurations()
        {
            ProvidersFilmConfiguration = new ProvidersFilmConfiguration();
        }

    }
}