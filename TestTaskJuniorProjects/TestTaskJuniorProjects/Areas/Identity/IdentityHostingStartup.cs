﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(TestTaskJuniorProjects.Areas.Identity.IdentityHostingStartup))]
namespace TestTaskJuniorProjects.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}