﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTaskJuniorProjects.Models;
using TestTaskJuniorProjects.Util;
using TestTaskJuniorProjects.ViewModel;

namespace JP.Infrastructure
{
    public interface IManagerMVC
    {
        /// <summary>
        /// Получение списка фильмов
        /// </summary>
        List<FilmVm> GetFilms();

        /// <summary>
        /// Получение списка фильмов c пагинацией
        /// </summary>
        PagedResult<Film> GetFilmsPagedResult(int page, int size);
        
        /// <summary>
        /// Получить данные о пользователе из db
        /// </summary>
        /// <param name="userID">ID пользователя</param>
        /// <returns></returns>
        AppUser GetUser(string userID);

        /// <summary>
        /// Наити фильм по его ID
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        Task<Film> GetFilmByID(int? id);

        /// <summary>
        /// Редактирование фильма
        /// </summary>
        /// <param name="film">Модель фильма</param>
        /// <param name="img">Модель для загрузки img</param>
        /// <param name="id">ID Записи</param>
        Task EditFilm(FilmVm film, Img img, int id);

        /// <summary>
        /// Загрузка img
        /// </summary>
        /// <param name="img">Модель для загрузки img</param>
        /// <returns></returns>
        string UploadImage(Img img);

        /// <summary>
        /// Создане фильма
        /// </summary>
        /// <param name="film">Модель фильма</param>
        /// <param name="img">Модель для загрузки img</param>
        /// <returns></returns>
        Task CreateFilm(FilmVm film, Img img, string userId);


        /// <summary>
        /// Удаление фильма
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        Task RemoveFilm(int? id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        bool FilmExists(int id);
    }
}