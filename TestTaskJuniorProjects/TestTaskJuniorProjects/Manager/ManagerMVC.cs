﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JP.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TestTaskJuniorProjects.Extensions;
using TestTaskJuniorProjects.Models;
using TestTaskJuniorProjects.Util;
using TestTaskJuniorProjects.ViewModel;

namespace TestTaskJuniorProjects.Manager
{
    /// <summary>
    /// Класс менеджер для работы с фильмами
    /// </summary>
    public class ManagerMVC : IManagerMVC
    {
        private readonly IUnitOfWork _uow;
        private readonly UserManager<AppUser> _userManager;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;

        public ManagerMVC(IUnitOfWork uow, UserManager<AppUser> userManager, FileUploadService fileUploadService, IHostingEnvironment environment)
        {
            _uow = uow;
            _userManager = userManager;
            _fileUploadService = fileUploadService;
            _environment = environment;
        }

        /// <summary>
        /// Получение списка фильмов
        /// </summary>
        public List<FilmVm> GetFilms()
        {
            var films = _uow.GetRepository<Film>().GetAll();
            var model = new List<FilmVm>();
            foreach (var item in films)
            {
                var film = new FilmVm()
                {
                    Description = item.Description,
                    Name = item.Name,
                    Director = item.Director,
                    ReleaseDate = item.ReleaseDate,
                    User = GetUser(item.UserId),
                    UserId = item.UserId,
                    ID = item.ID,
                    ImgUrl = item.ImgUrl
                };
                model.Add(film);
                
            }

            return model;
        }
        
        /// <summary>
        /// Получение списка фильмов c пагинацией
        /// </summary>
        public PagedResult<Film> GetFilmsPagedResult(int page, int size)
        {
            var films =_uow.GetRepository<Film>().GetAll()
                .OrderByDescending(x => x.ID)
                .GetPaged(page, size);

            foreach (var film in films.Results)
            {
                film.User = GetUser(film.UserId);
            }

            return films;
        }
        
        /// <summary>
        /// Получить данные о пользователе из db
        /// </summary>
        /// <param name="userID">ID пользователя</param>
        /// <returns></returns>
        public AppUser GetUser(string userID)
        {
            return _uow.GetRepository<AppUser>().GetAll().FirstOrDefault(x => x.Id == userID);
        }

        /// <summary>
        /// Наити фильм по его ID
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        public Task<Film> GetFilmByID(int? id)
        {
            return _uow.GetRepository<Film>().GetAll().SingleOrDefaultAsync(m => m.ID == id);
        }
        
        /// <summary>
        /// Редактирование фильма
        /// </summary>
        /// <param name="film">Модель фильма</param>
        /// <param name="img">Модель для загрузки img</param>
        /// <param name="id">ID Записи</param>
        public async Task EditFilm(FilmVm film, Img img, int id)
        {
            var searching = GetFilmByID(id).Result;
            
            var imageUrlContent = img.ImgUrl == null ? searching.ImgUrl : UploadImage(img);

            searching.Name = film.Name;
            searching.Director = film.Director;
            searching.ReleaseDate = film.ReleaseDate;
            searching.Description = film.Description;
            searching.ImgUrl = imageUrlContent;

            _uow.GetRepository<Film>().Update(searching);
            _uow.Complete();
        }

        /// <summary>
        /// Создане фильма
        /// </summary>
        /// <param name="film">Модель фильма</param>
        /// <param name="img">Модель для загрузки img</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task CreateFilm(FilmVm film, Img img, string userId)
        {
            var imageUrlContent = UploadImage(img);

            var fModel = new Film()
            {
                Name = film.Name,
                Director = film.Director,
                Description = film.Description,
                ImgUrl = imageUrlContent,
                UserId = userId,
                ReleaseDate = film.ReleaseDate
            };

            _uow.GetRepository<Film>().Add(fModel);
            _uow.Complete();
        }

        /// <summary>
        /// Удаление фильма
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        public async Task RemoveFilm(int? id)
        {
            var film = await GetFilmByID(id);
            _uow.GetRepository<Film>().Remove(film);
            _uow.Complete();
            DeleteImage(film.ImgUrl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">ID Записи</param>
        /// <returns></returns>
        public bool FilmExists(int id)
        {
            return _uow.GetRepository<Film>().GetAll().Any(e => e.ID == id);
        }

        /// <summary>
        /// Загрузка img
        /// </summary>
        /// <param name="model">Модель для загрузки img</param>
        /// <returns></returns>
        public string UploadImage(Img model)
        {
            var guid = Guid.NewGuid();
            var path = Path.Combine(_environment.WebRootPath, $"assets\\image");
            _fileUploadService.Upload(path, guid + "_"+ model.ImgUrl.FileName, model.ImgUrl);
            var imageUrlContent = $"assets\\image\\{guid}_{model.ImgUrl.FileName}";
            return imageUrlContent;
        }
        
        /// <summary>
        /// Удаление img
        /// </summary>
        /// <param name="imgFile">Имя файла для удаления</param>
        /// <returns></returns>
        public void DeleteImage(string imgFile)
        {
            var path = Path.Combine(_environment.WebRootPath, imgFile);
            _fileUploadService.Delete(path);
        }
    }
}