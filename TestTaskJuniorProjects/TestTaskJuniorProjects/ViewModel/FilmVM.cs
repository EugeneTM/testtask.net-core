﻿using System;
using System.ComponentModel.DataAnnotations;
using TestTaskJuniorProjects.Attributes;
using TestTaskJuniorProjects.Models;

namespace TestTaskJuniorProjects.ViewModel
{
    public class FilmVm
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// Наименование фильма
        /// </summary>
        [Display(Name = "Наименование фильма")]
        [Required (ErrorMessage = "Не указано имя")]
        public string Name { get; set; }
        
        /// <summary>
        /// Режесёр 
        /// </summary>
        [Display(Name = "Режесёр")]
        [Required (ErrorMessage = "Режесёр не указан")]
        public string Director { get; set; }
        
        /// <summary>
        /// Дата выхода
        /// </summary>
        [Display(Name = "Дата релиза")]
        [ValidateDate("Дата релиза указана неверно.")]
        public DateTime ReleaseDate { get; set; }
        
        /// <summary>
        /// Постер
        /// </summary>
        [Display(Name = "Изображение")]
        public string ImgUrl { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [Display(Name = "Описание")]
        [Required (ErrorMessage = "Введите описание")]
        public string Description { get; set; }
        
        /// <summary>
        /// ID Пользователя
        /// </summary>
        public string UserId { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        public AppUser User { get; set; }
    }
}