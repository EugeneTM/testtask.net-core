﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TestTaskJuniorProjects.ViewModel
{
    /// <summary>
    /// Модель для работы с изображением
    /// </summary>
    public class Img
    {
        /// <summary>
        /// Изображение
        /// </summary>
        [Display(Name = "Изображение")]
        public IFormFile ImgUrl { get; set; }
    }
}