﻿using System.Collections;
using System.Collections.Generic;
using TestTaskJuniorProjects.Models;
using TestTaskJuniorProjects.Util;

namespace TestTaskJuniorProjects.ViewModel
{
    public class FilmViewModel
    {
        public PagedResult<Film> Films { get; set; }
        // public PageBaseModel PageBaseModel { get; set; }
    }
}